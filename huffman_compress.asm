; Janosi Jozsef - Hunor
; Huffman Compressor


; nasm -fobj huffman_compress.asm
; alink -oPE huffman_compress.obj
; huffman_compress.exe 0 alma


; fc /b file1 file2 - compare

extern FindFirstFileA 
extern FindNextFileA 
extern FindClose 
extern GetLastError
extern SetCurrentDirectoryA
extern GetCurrentDirectoryA
extern CreateDirectoryA
extern fio_open
extern fio_close
extern fio_read
extern fio_write
extern getargs

import FindFirstFileA kernel32.dll 
import FindNextFileA kernel32.dll 
import FindClose kernel32.dll 
import GetLastError kernel32.dll 
import SetCurrentDirectoryA kernel32.dll
import GetCurrentDirectoryA kernel32.dll
import CreateDirectoryA kernel32.dll
import fio_open util.dll
import fio_close util.dll
import fio_read util.dll
import fio_write util.dll
import getargs util.dll


global main

section .text use32

..start:
	call	main
	ret

sequence:
	pusha
	
	xor		edi, edi	; how many characters
	xor		esi, esi	; how many symbols
	
	mov		edx, 1
	mov		ecx, 1
	mov		ebx, betu
	mov		eax, [fileHandle_in]
	call	[fio_read]
.loop:
	test 	edx, edx
	jz		.end
	
	inc		edi
	cmp		edi, 0x7fffffff		; if file too big
	je		.end
	
	; seq[c]++
	mov		eax, [betu]
	mov		ebx, 4
	mul		ebx
	mov		edx, DWORD [sequences + eax]
	test	edx, edx
	jz		.inc
.inc_back:
	inc		edx
	mov		DWORD [sequences + eax], edx
	
	mov		ebx, betu
	mov		eax, [fileHandle_in]
	call	[fio_read]
	jmp		.loop
.end:
	mov		eax, [fileHandle_in]
	call	[fio_close]
	
	mov		DWORD [betu], esi
	mov		DWORD [temp], edi
	
	popa
	ret

.inc:
	inc		esi
	jmp		.inc_back
	
	
doTree:
	push	eax
	; push	ebx
	push	ecx
	push	edx
	push	esi
	push	edi
	
	mov		edx, -1
	mov		ecx, 256
	mov		eax, 1024 	; 4*256
.ciklus0:
	mov		DWORD [tree0 + eax], edx
	sub		eax, 4
	dec		ecx
	; loop	.ciklus0
	cmp		ecx, -2
	jg		.ciklus0
	; -1 for upper row
	
	mov		ecx, 256
	mov		eax, 1024 	; 4*256
.ciklus1:
	mov		DWORD [tree1 + eax], edx
	sub		eax, 4
	dec		ecx
	; loop	.ciklus1
	cmp		ecx, -2
	jg		.ciklus1
	; -1 for lower row
	

	mov		ecx, 511
	mov		eax, 2044
	xor		edx, edx
.ciklus00:
	mov		DWORD [tree0 + eax], edx
	sub		eax, 4
	dec		ecx
	
	cmp		ecx, 256
	jg		.ciklus00
	; 0 for upper row
	
	mov		ecx, 511
	mov		eax, 2044 	; 4*512
.ciklus01:
	mov		DWORD [tree1 + eax], edx
	sub		eax, 4
	dec		ecx
	
	cmp		ecx, 256
	jg		.ciklus01
	; 0zva  az also sor
	
	
	mov		ebx, 1020 ; 256*4-4 - j - where the new items will go
	mov		DWORD [sequences + 2044], 0x7fffffff ; biggest number
	
.ciklus2:
	mov		eax, 2044 	; 4*511
	mov		DWORD [min1], eax
	mov		DWORD [min2], 2044
	mov		ecx, 511
.ciklus_min:
	dec		ecx
	sub		eax, 4
	cmp		ecx, -1
	je		.place_min
	
	mov		edx, DWORD [sequences + eax]
	test	edx, edx 	; if nothing here
	jz		.ciklus_min
	
	mov		edi, DWORD [min1]
	mov		esi, DWORD [sequences + edi]
	cmp		edx, esi
	jl		.min_1
	mov		edi, DWORD [min2]
	mov		esi, DWORD [sequences + edi]
	cmp		edx, esi
	jl		.min_2
	jmp		.ciklus_min

.min_1:
	mov		edx, DWORD [min1]
	mov		DWORD [min2], edx
	mov		DWORD [min1], eax
	jmp		.ciklus_min
.min_2:
	mov		DWORD [min2], eax
	jmp		.ciklus_min

.place_min:
	mov		esi, DWORD [min1]
	cmp		esi, 2044
	je		.end
	mov		esi, DWORD [min2]
	cmp		esi, 2044
	je		.egy_van

	add		ebx, 4 	;j++
	mov		DWORD [tree1 + ebx], esi  	; p[j][1]=min2.
	
	mov		eax, DWORD [sequences + esi] ; seq[min2]
	mov		DWORD [sequences + esi], 0 	; seq[min2] = 0;used
	
	mov		esi, DWORD [min1]
	mov		DWORD [tree0 + ebx], esi 	; p[j][0]=min1. ; new item's leaves
	
	mov		ecx, DWORD [sequences + esi] ; seq[min1]
	mov		DWORD [sequences + esi], 0 	; seq[min1] = 0; used
	
	add		eax, ecx ; seq[min1]+seq[min2]
	mov		DWORD [sequences + ebx], eax ; seq[j] = seq[min1]+seq[min2] ; new item in seq array
	
	jmp		.ciklus2
	
.egy_van:
	cmp		ebx, 1020
	jne		.end
	mov		esi, DWORD [min1]
	add		ebx, 4
	mov		DWORD [tree0 + ebx], esi 	; p[j][0]=min1. ; new item's leaves
	
	add		esi, 4
	mov		DWORD [tree1 + ebx], esi  	; p[j][1]= *imaginary elem*
	sub		esi, 4
	; szimbolumok szama++
	mov		eax, DWORD [betu]
	inc		eax
	mov		DWORD [betu], eax
	
	mov		DWORD [sequences + esi], 0 	; seq[min1] = 0; used
	jmp		.end_pop
	
.null_elem:
	add		ebx, 4
	mov		DWORD [tree0 + ebx], 0
	mov		DWORD [tree1 + ebx], 4
	; szimbolu szama += 2
	mov		eax, DWORD [betu]
	add		eax, 2
	mov		DWORD [betu], eax
	
.end:
	cmp		ebx, 1020		; if there was no item
	je		.null_elem
	; sequences = null
	xor		ecx, ecx
.seq0:
	cmp		ecx, 2048
	je		.end_pop
	
	mov		DWORD [sequences + ecx], 0
	add		ecx, 4
	jmp		.seq0
	
.end_pop:
	pop		edi
	pop		esi
	pop		edx
	pop		ecx
	; pop		ebx	; parameter
	pop		eax
	ret	

	
doChar:
	push	eax
	push	ebx
	push	ecx
	push	edx

    ; character's code recursively
	mov		eax, DWORD [tree0 + ebx] 	; last item's left leaf
	cmp		eax, -1
	jle		.end
	
	mov		eax, '0'
	stosb
	
	mov		edx, ebx
	mov		ebx, DWORD [tree0 + ebx] 	; left leaf is set by parameter
	call	doChar
	
	mov		ebx, edx
	dec		edi		; erase '0'
	
	mov		eax, '1'
	stosb
	
	mov		ebx, DWORD [tree1 + ebx]
	call	doChar
	
	mov		ebx, edx
	dec		edi 	; erase '1'
	
	jmp		.end_end_end
	
.end:
	push	edi
	push	esi
	
	mov		BYTE [edi], 0
	mov		edx, 256
	mov		eax, ebx	; j = ebx
	mul		edx			; s[j]
	mov		edi, 4
	cdq
	div		edi
	mov		ecx, char
	add 	ecx, eax
	mov		edi, ecx
.masol:
	movsb
	movzx	eax, BYTE [esi]
	test	eax, eax
	jz		.end_end
	jmp		.masol
	
.end_end:
	mov		eax, ebx
	mov		ecx, 4
	cdq
	div		ecx
	
	; call	mio_writechar
	push	ebx
	mov		BYTE [min2], al
	mov		ecx, 1		; bytes to write
	mov		ebx, min2	; where to read 'em from
	mov		eax, [fileHandle_out]; where to write 'em
	call	[fio_write]	; write 'em
	pop		ebx
	
	mov		eax, ebx
	mov		edx, 256
	mul		edx
	mov		ecx, 4
	cdq
	div		ecx
	add		eax, char
	
	; call	io_writestr
	push	ebx
	mov		ebx, eax	; where to read 'em from
.write_cicle:
	movzx	ecx, BYTE [ebx]
	test	ecx, ecx
	jz		.write_end
	mov		ecx, 1		; bytes to write
	
	mov		eax, [fileHandle_out]; where to write 'em
	call	[fio_write]	; write 'em
	
	inc		ebx
	jmp		.write_cicle
.write_end:
	mov		ecx, DWORD ' '
	mov		DWORD [min1], ecx
	mov		ecx, 1
	mov		ebx, min1
	call	[fio_write]
	pop		ebx
	
	; mov		eax, ' '
	; call	mio_writechar ; debug write out
	
	pop		esi
	pop		edi
.end_end_end:
	pop		edx
	pop		ecx
	pop		ebx
	pop		eax
	ret	
	
	
	
StrLen:		; ESI > EAX
	push	esi
	push	ecx
	xor 	ecx, ecx
.start:
	lodsb
	test	al, al
	jz		.end
	
	inc		ecx
	jmp		.start
.end:
	mov		eax, ecx
	pop		ecx
	pop		esi
	ret

StrCat: 	; ESI, EDI > EDI   (EDI+ESI)
	push	esi
	push	edi
	push	eax
	
	xchg	esi, edi
; walk through edi
.Walk:
	lodsb
	test	al, al
	jz		.Cat
	
	jmp		.Walk
.Cat:
	dec		esi
	xchg	esi, edi
.CatStart:
	lodsb
	stosb
	test	al, al
	jz		.end
	
	jmp		.CatStart
.end:
	pop		eax
	pop		edi
	pop		esi
	ret
	
	
out_code:
	pusha

    xor		eax, eax
	mov		DWORD [temp], eax 	; nextByte
	
	mov		edx, 1		; bytes read
	mov		ecx, 1		; bytes to read
	mov		ebx, betu	; where to read 'em
	mov		eax, [fileHandle_in]; from where to read 'em
	call	[fio_read]	; read 'em
	
	xor		ecx, ecx 	; bitCount
	
.ciklus1:		; one byte read in
	test 	edx, edx
	jz		.end
	
	mov		ebx, DWORD [betu]
	cmp		ebx, 0
	jle		.neg
.cont:			; ha nem negativ
	mov		eax, ebx
	mov		edi, 256
	mul		edi
	mov		esi, eax
	add		esi, char 	; *char[c]
	
	call	StrLen		; ESI > EAX
	
	xor		edi, edi
.ciklus2:		; shifts bits, and if 8 bits arrive-> write out
	cmp		edi, eax 	; i < meret[s[c]]
	je		.ciklus2_end
	
	cmp		ecx, 8 	; if can write out
	jne		.not_enough
	;write temp
	
	push	eax 		; string's length
	mov		ecx, 1		; bytes to write
	mov		ebx, temp	; where to read 'em from
	mov		eax, [fileHandle_out]; where to write 'em
	call	[fio_write]	; write 'em
	pop		eax
	
	xor		ebx, ebx
	mov		DWORD [temp], ebx
	xor		ecx, ecx
.not_enough:
	movzx	edx, BYTE [esi]
	cmp		edx, '1'
	jne		.notOne
	mov		edx, 1
	shl		edx, cl
	mov		ebx, DWORD [temp]
	or		ebx, edx
	mov		DWORD [temp], ebx
.notOne:
	inc		esi
	inc		edi		; i++
	inc		ecx		; bitCount++
	jmp		.ciklus2
.ciklus2_end:
	mov		edx, 1		; bytes read
	push	ecx 	; bitCount
	mov		ecx, 1		; bytes to read
	mov		ebx, betu	; where to read 'em
	mov		eax, [fileHandle_in]; from where to read 'em
	call	[fio_read]	; read 'em
	pop		ecx	 	; bitCount
	
	jmp		.ciklus1
	
.end:
	test	ecx, ecx	; if bits left
	jz		.end_end
	
	mov		ecx, 1		; bytes to write
	mov		ebx, temp	; where to read 'em from
	mov		eax, [fileHandle_out]; where to write 'em
	call	[fio_write]	; write 'em
	
.end_end:
	mov		eax, [fileHandle_in]
	call	[fio_close]
	
.popz:
	popa
	ret
.neg:
	neg		ebx
	jmp		.cont
.error:
	; mov		eax, errormsg
	; call	io_writestr
	; mov		eax, 10
	; call	mio_writechar
	jmp		.popz

openFile:
	pusha
	
	mov		eax, DWORD [filename_in]
	xor		ebx, ebx
	call	[fio_open]
	mov		DWORD [fileHandle_in], eax
	
	mov		esi, DWORD [filename_in]
	call	StrLen
	mov 	DWORD [hossz], eax
	
	mov		edi, [filename_in]
	mov		esi, filename_out
	call	StrCat
	mov		eax, edi 	; filename_in.encoded concatenate
	;mov		eax, filename_out
	mov		ebx, 1 	; 1-write
	call	[fio_open]
	mov		[fileHandle_out], eax
	
	mov		eax, [fileHandle_out]
	mov		ebx, hossz
	mov		ecx, 1
	call	[fio_write]
	
	; out: filename
	mov		edi, DWORD [hossz]
	mov		ebx, DWORD [filename_in]
	mov		ecx, edi
	call	[fio_write]
	
	
	; out: not folder - 0
	mov		eax, [fileHandle_out]
	mov		ecx, 1
	mov		DWORD [min1], 0
	mov		ebx, min1
	call	[fio_write]
	
	
	
.szimbolum:
	; out: how many symbols
	mov		ebx, betu
	mov		ecx, 2
	call	[fio_write]
	
	; out: how many characters overall
	mov		ecx, 4
	mov		ebx, temp
	call	[fio_write]
	xor		ecx, ecx
	mov		DWORD [betu], ecx
	mov		ecx, 1
	
	mov		edx, ' '
	mov		DWORD [betu], edx
	mov		ebx, betu
	call	[fio_write]
	
	xor		eax, eax
	mov		DWORD [temp], eax
	mov		DWORD [betu], eax
	
	popa
	ret
	
folderOpenFile:
	pusha

	mov		esi, DWORD [filename_in]
	call	StrLen
	mov 	DWORD [hossz], eax
	
	mov		edi, [filename_in]
	mov		esi, filename_out
	call	StrCat
	
	mov		eax, edi 	; filename_in.encoded concatenate
	mov		ebx, 1 	; 1-write
	call	[fio_open]
	mov		[fileHandle_out], eax
	
	mov		eax, [fileHandle_out]
	mov		ebx, hossz
	mov		ecx, 1
	call	[fio_write]
	
	; out: filename
	mov		edi, DWORD [hossz]
	mov		ebx, DWORD [filename_in]
	mov		ecx, edi
	call	[fio_write]
	
	popa
	ret
	
dirChange:
	pusha
	
	mov		eax, directory
	push	eax
	push	256
	call	[GetCurrentDirectoryA]
	
	mov		DWORD [string], '\'
	
	mov		esi, string
	mov		edi, edx
	call	StrCat		; curDir/
	
	mov		esi, edi
	call	StrLen		; curDir length
	
	mov		DWORD [betu], eax
	
	mov		esi, [filename_in]
	call	StrCat		; curDir/filename
	
	add		edi, eax
	mov		eax, DWORD [hossz] 	; filename hossz
	add		edi, eax
	mov		DWORD [edi], 0
	sub		edi, eax
	mov		eax, DWORD [betu]
	sub		edi, eax
	
	
	push 	edi
	call 	[SetCurrentDirectoryA]
	
	popa
	ret

	
nullChar:
	push	ecx
	xor		ecx, ecx
.ciklus:
	cmp		ecx, 65536
	je		.end
	mov		DWORD [char + ecx], 0
	add		ecx, 4
	jmp		.ciklus
	
.end:
	pop		ecx
	ret
	
folderOpenFile2:
	pusha
	
	mov		eax, DWORD [filename_in]
	xor		ebx, ebx
	call	[fio_open]
	mov		DWORD [fileHandle_in], eax
	
	mov		esi, DWORD [filename_in]
	call	StrLen
	mov 	DWORD [hossz], eax
	
	; out: hossz
	mov		eax, [fileHandle_out]
	mov		ebx, hossz
	mov		ecx, 1
	call	[fio_write]
	
	; out: filename
	mov		edi, DWORD [hossz]
	mov		ebx, DWORD [filename_in]
	mov		ecx, edi
	call	[fio_write]
	
	; out: not folder - 0
	mov		eax, [fileHandle_out]
	mov		ecx, 1
	mov		DWORD [min1], 0
	mov		ebx, min1
	call	[fio_write]
	
	
.szimbolum:
	; out: how many symbols
	mov		ebx, betu
	mov		ecx, 2
	call	[fio_write]
	
	; out: how many characters overall
	mov		ecx, 4
	mov		ebx, temp
	call	[fio_write]
	xor		ecx, ecx
	mov		DWORD [betu], ecx
	mov		ecx, 1
	
	mov		edx, ' '
	mov		DWORD [betu], edx
	mov		ebx, betu
	call	[fio_write]
	
	xor		eax, eax
	mov		DWORD [temp], eax
	mov		DWORD [betu], eax
	
	popa
	ret
	
megszamol:
	pusha
	
	xor		eax, eax
	mov		DWORD [szam], eax
	mov		DWORD [szam + 4], eax
	
	push	DWORD file_attributes
	push	file_search
	call	[FindFirstFileA]
	mov		DWORD [dirhandle], eax
	; .
	
	push	DWORD file_attributes
	push	DWORD [dirhandle]
	call	[FindNextFileA]
	; ..
	
.count:
	push	DWORD file_attributes
	push	DWORD [dirhandle]
	call	[FindNextFileA]
	
	
	; pusha
	; push	directory
	; push	256
	; call	[GetCurrentDirectoryA]
	; mov		esi, edx
	; call	StrLen
	; mov		esi, file_attributes
	; add		esi, 44
	; mov		esi, edx
	; call	StrLen
	; mov		ecx, eax
	; mov		DWORD [string], eax
	; mov		eax, DWORD [dunno]
	; inc		eax
	; mov		DWORD [dunno], eax
	; add		eax, '0'
	; mov		eax, Yofilename
	; mov		ebx, 1
	; call	[fio_open]
	; mov		ecx, 1
	; mov		ebx, string
	; call	[fio_write]
	; call	[fio_close]
	; popa
	
	test	eax, eax 	; no files left
	jz		.might_end
.not_end:
	
	mov		eax, DWORD [szam]
	inc		eax
	mov		DWORD [szam], eax
	
	jmp		.count
	
.write:
	mov		eax, [fileHandle_out]
	mov		ebx, szam
	mov		ecx, 4			; 4 bytes are written out, so a lot of files can fit in
	call	[fio_write]
	
	popa
	ret
	
.might_end:
	call 	[GetLastError]
	cmp 	eax, 18
	je		.write
	jmp		.not_end
	
folderComp:
	pusha
	
	; loop while no more files. compress them. Write into the file.
	; out: folder - 1
	mov		eax, [fileHandle_out]
	mov		ecx, 1
	mov		DWORD [betu], ecx
	mov		ebx, betu
	call	[fio_write]
	
	call	megszamol		; counts how many files in the folder and writes out
	
	push	DWORD file_attributes
	push	file_search
	call	[FindFirstFileA]
	mov		DWORD [dirhandle], eax
	; .
	
	push	DWORD file_attributes
	push	DWORD [dirhandle]
	call	[FindNextFileA]
	; ..
	
.new_file:
	push	DWORD file_attributes
	push	DWORD [dirhandle]
	call	[FindNextFileA]
	; first file
	
	test 	eax, eax
	jz		.might_end
	
.not_end:
	cmp		DWORD [file_attributes], 0x10
	je		.folder
	
	; open file
	mov		eax, file_attributes
	add		eax, 44
	mov		DWORD [filename_in], eax
	xor		ebx, ebx
	call	[fio_open]
	mov		[fileHandle_in], eax
	
	
	call	sequence 	; sequence array for the symbols in the file
	
	call	doTree		; tree creation ; out:ebx: which is the last item
	
	call	nullChar	; char array = null
	
	call	folderOpenFile2
	
	mov		esi, temp
	mov		edi, temp
	call	doChar
	
	call	out_code
	
	jmp		.new_file
.end:
	push	DWORD [dirhandle]
	call	[FindClose]
	popa
	ret
.might_end:
	call 	[GetLastError]
	cmp 	eax, 18
	je		.end
	jmp		.not_end
	
.folder:
	; save path so we can return. Change dir. And call this function
	; write out hossz and namefile
	
	push	DWORD [dirhandle]	
	
	xor		eax, eax
	mov		DWORD [hossz], eax
	
	mov		esi, file_attributes
	add		esi, 44			; filename
	call	StrLen
	mov		DWORD [hossz], eax
	
	mov		eax, [fileHandle_out]
	mov		ebx, hossz
	mov		ecx, 1
	call	[fio_write]

	; out: filename
	mov		ecx, DWORD [hossz]
	mov		ebx, esi
	call	[fio_write]
	
	; save path
	mov		eax, directory
	push	eax
	push	256
	call	[GetCurrentDirectoryA]
	
	;push	DWORD [dirhandle]	; pushed so previous search isnt lost
	push	edx					; path is pushed in
	
	mov		esi, edx
	call	StrLen
	push	eax
	
	; change dir
	mov		DWORD [string], '\'
	
	mov		esi, string
	mov		edi, edx
	call	StrCat		; curDir/
	
	mov		esi, edi
	call	StrLen		; curDir length
	
	mov		DWORD [betu], eax
	
	mov		esi, file_attributes		; filename
	add		esi, 44
	call	StrCat		; curDir/filename
	
	add		edi, eax
	mov		eax, DWORD [hossz] 	; filename hossz
	add		edi, eax
	mov		DWORD [edi], 0
	sub		edi, eax
	mov		eax, DWORD [betu]
	sub		edi, eax
	
	push 	edi
	call 	[SetCurrentDirectoryA]
	
	; compression
	call	folderComp
	
	; change back the dir.
	; path is pushed in
	pop		eax
	pop		edi
	
	; mov		ecx, eax
	mov		ecx, eax
	; sub		ecx, 8
	xor		edx, edx
	
	mov		DWORD [temp], 0
	
.loopz:
	mov		bl, BYTE [edi + edx]
	mov		BYTE [temp + edx], bl
	add		edx, 1
	loop	.loopz
	
	mov		DWORD [temp + eax], 0
	mov		DWORD [temp + eax + 4], 0
	
	push	temp
	call	[SetCurrentDirectoryA]
	
	; pop back the dirhandle and file_attributes for the previous searches
	pop		DWORD [dirhandle]
	
	jmp		.new_file
	
compress:
	; see if the file is folder or not
	
	push	DWORD file_attributes
	push	DWORD [filename_in]		; filename
	call	[FindFirstFileA]
	
	mov		esi, file_attributes
	cmp		DWORD [esi], 0x10
	je		.folder
	
	; open file to read from
	mov		eax, DWORD [filename_in]
	xor		ebx, ebx 	; 0 -read
	call	[fio_open]
	mov		[fileHandle_in], eax
	
	call	sequence

	call	doTree
	
	call	openFile	; opens output file and writes out the name of the file and 2 numbers
	
	mov		esi, temp
	mov		edi, temp
	call 	doChar		; code creation ; in:ebx, esi, edi

	call	out_code
.ret:
	ret
	
.folder:
	; open output file
	call	folderOpenFile
	; go into the folder
	call	dirChange
	; compress the files in it
	call	folderComp
	
	mov		eax, [fileHandle_out]
	call	[fio_close]
	
	jmp		.ret
	
openDecompFile:
	pusha
	
	xor		eax, eax
	mov		DWORD [betu], eax
	; read no. of chars of the name
	mov		eax, [fileHandle_in] ; from where
	mov		ebx, betu	; where to
	mov		ecx, 1 		; how many
	call	[fio_read]
	test	edx, edx
	jz		.vege
	
	; read the filename
	xor		ecx, ecx
	mov		DWORD [filename], ecx
	mov		DWORD [filename + 4], ecx
	mov		ebx, filename
	mov		ecx, DWORD [betu]
	call	[fio_read]
	
	mov		eax, DWORD [filename]
	inc		eax
	mov		DWORD [filename], eax
	mov		DWORD [filename + ecx], 0
	
	; read if folder 0/1
	mov		ebx, hossz
	mov		ecx, 1
	mov		eax, [fileHandle_in]
	call	[fio_read]
	
	cmp		DWORD [hossz], 1
	je		.folder
	
	; open output file
	mov		eax, filename
	
	add		eax, DWORD [betu]
	mov		DWORD [eax], 0
	sub		eax, DWORD [betu]
	mov		ebx, 1 		; 1-write
	call	[fio_open]
	mov		[fileHandle_out], eax
	
	clc
.ret:
	popa
	ret
	
.folder:
	; create directory
	push 	DWORD 0
	push	DWORD filename
	call	[CreateDirectoryA]
	; go into the directory
	
	mov		eax, directory
	push	eax
	push	256
	call	[GetCurrentDirectoryA]
	
	push	edx		; because of multiplication it goes wrong
	; push	eax			; pushed in the length
	mov		eax, DWORD [folderszam]
	mov		ebx, 4
	mul		ebx
	mov		ebx, eax
	pop		edx
	
	; save the length of curDir
	mov		esi, edx
	call	StrLen
	
	mov		DWORD [char + ebx], eax
	; a HOSSZA
	
	
	mov		DWORD [string], '\'
	
	mov		esi, string
	mov		edi, edx
	call	StrCat		; curDir/
	
	mov		esi, edi
	call	StrLen		; curDir length
	
	mov		DWORD [betu], eax
	
	mov		esi, filename
	call	StrCat		; curDir/filename
	
	push 	edi
	call 	[SetCurrentDirectoryA]
	
	xor		eax, eax
	mov		DWORD [filename], eax

	; save the current fileCounter
	; push	DWORD [szam]
	
	push	edx
	mov		eax, DWORD [folderszam]
	mov		ebx, 4
	mul		ebx
	mov		ebx, eax
	add		ebx, 1024		; put it to the second half of char
	mov		eax, DWORD [szam]
	pop		edx
	
	; save the fileCounter
	mov		DWORD [char + ebx], eax
	
	; increment the folderCounter
	mov		eax, DWORD [folderszam]
	inc		eax
	mov		DWORD [folderszam], eax
	
	
	; read how many files are in the folder
	mov		eax, [fileHandle_in]
	mov		ebx, szam
	mov		ecx, 4
	call	[fio_read]
	
	clc
	jmp		.ret
.vege:		; end of file
	stc
	jmp		.ret
	

decompTree:
	pusha
	
	; read no. of symbols
	mov		eax, [fileHandle_in] ; from where
	mov		ebx, betu	; where to
	mov		ecx, 2 		; how many
	call	[fio_read]

	; read no. of characters to write out
	mov		ecx, 4
	mov		ebx, min2
	call	[fio_read]
	
	mov		edi, DWORD [betu] 	; the number of symbols

	; Put -1 onto the tree: 256 + the number of symbols, so you know, you haven't been there
	push	edi
	mov		eax, 1024
.minusOne:
	test	edi, edi
	jz		.minusOneEnd
	
	mov		DWORD [tree0 + eax], -1
	mov		DWORD [tree1 + eax], -1
	
	add		eax, 4
	dec		edi
	jmp		.minusOne
.minusOneEnd:
	pop		edi
	
	mov		eax, [fileHandle_in]
	mov		ebx, betu
	mov		ecx, 1
	call	[fio_read]		; space
.readLoop:
	test	edi, edi
	jz		.readEnd
	
	; read in symbol
	xor		eax, eax
	mov		DWORD [betu], eax
	mov		eax, [fileHandle_in] ; from where
	mov		ebx, betu	; where to
	mov		ecx, 1 		; how many
	call	[fio_read]
	
	; now put -1 to the leaf
	mov		eax, DWORD [betu]
	mov		esi, 4
	mul		esi
	mov		DWORD [betu], eax	; betu*4
	mov		DWORD [tree0 + eax], -1
	mov		DWORD [tree1 + eax], -1
	
	; read the codes until <space> and grow the tree
	mov		esi, 1024	; the root
.readCode:
	mov		eax, [fileHandle_in]
	mov		ebx, temp
	call	[fio_read]
	
	cmp		DWORD [temp], ' '
	je		.readCodeEnd
	
	mov		ebx, DWORD [temp]
	sub		ebx, '0'
	test	ebx, ebx
	jz		.zero
	
.one:
	cmp		DWORD [tree1 + esi], -1
	jne		.oneDownTheTree 	; it exists already
	push	esi
	; search for the next free branch
.oneLoop:
	add		esi, 4 		; next possible branch
	cmp		DWORD [tree0 + esi], -1
	jne		.oneLoop
	cmp		DWORD [tree1 + esi], -1
	jne		.oneLoop
	
	; if free
	; found it. now place it
	pop		ebx 	; the place for it
	mov		eax, tree1
	add		eax, ebx
	mov		DWORD [min1], eax		; where I put last. So if the end comes, i know where to put the leaf
	mov		DWORD [tree1 + ebx], esi 	; pointing to the free branch, from the root
	
	jmp		.readCode
.oneDownTheTree:
	mov		esi, DWORD [tree1 + esi]
	jmp		.readCode
	
	
.zero:
	cmp		DWORD [tree0 + esi], -1
	jne		.zeroDownTheTree 	; it exists already
	push	esi
	; search for the next free branch
.zeroLoop:
	add		esi, 4 		; next possible branch
	cmp		DWORD [tree0 + esi], -1
	jne		.zeroLoop
	cmp		DWORD [tree1 + esi], -1
	jne		.zeroLoop
	
	; if free
	; found it. now place it
	pop		ebx 	; the place for it
	mov		eax, tree0
	add		eax, ebx
	mov		DWORD [min1], eax		; where I put last. So if the end comes, i know where to put the leaf
	mov		DWORD [tree0 + ebx], esi 	; pointing to the free branch, from the root
	
	jmp		.readCode
.zeroDownTheTree:
	mov		esi, DWORD [tree0 + esi]
	jmp		.readCode

.readCodeEnd:
	; point the last branch to the leaf
	mov		eax, DWORD [min1]	; last used branch
	mov		ebx, DWORD [betu] 	; the symbol*4
	mov		DWORD [eax], ebx
	
	dec		edi
	jmp		.readLoop
.readEnd:
	popa
	ret

actDecomp:
	pusha
	; read in encoded data and decode it w/ the help of the tree
	
	cmp		DWORD [min2], 0
	je		.end
	
	mov		esi, 1024 	; root of the tree
.readLoop:
	xor		eax, eax
	mov		DWORD [betu], eax		; so no leftover is remaining
	
	mov		eax, [fileHandle_in]
	mov		ebx, betu
	mov		ecx, 1
	call	[fio_read]
	
	test	edx, edx		; check if EOF
	jz		.end
	
	xor		ecx, ecx	; one symbol = 8 bit$
.eightBitLoop:
	cmp		ecx, 8
	je		.readLoop
	
	mov		edi, DWORD [betu]
	shr		edi, cl
	and		edi, 1
	inc		ecx		; i++
	test	edi, edi
	jz		.zero
; .one:
	mov		esi, DWORD [tree1 + esi]
.back:
	cmp		DWORD [tree0 + esi], -1 	; if leaf
	je		.write
	jmp		.eightBitLoop
.zero:
	mov		esi, DWORD [tree0 + esi]
	jmp		.back
.write:
	xor		eax, eax
	mov		DWORD [temp], eax	; no leftover
	
	mov		eax, esi
	mov		esi, 4
	cdq
	div		esi		; symbol / 4
	mov		DWORD [temp], eax
	
	push	ecx		; this is the loopCounter
	mov		eax, [fileHandle_out]
	mov		ebx, temp
	mov		ecx, 1
	call	[fio_write]
	
	; dec [min2] ie. char counter
	mov		ecx, DWORD [min2]
	dec		ecx
	test	ecx, ecx
	jz		.pop_ecx_end
	mov		DWORD [min2], ecx
	
	pop		ecx
	
	mov		esi, 1024		; back to root
	
	jmp		.eightBitLoop
	
.end:
	; close files
	mov		eax, [fileHandle_out]
	call	[fio_close]
	
	popa
	ret
.pop_ecx_end:
	pop		ecx
	jmp		.end
	
	
decompress:
	; open the reader file
	mov		eax, DWORD [filename_in]
	xor		ebx, ebx 	; 0 -read
	call	[fio_open]
	mov		[fileHandle_in], eax
	
	mov		DWORD [szam], 1
	
.lol:
	; decrement the fileCounter
	mov		eax, DWORD [szam]
	dec		eax
	mov		DWORD [szam], eax
	
	cmp		eax, -1
	je		.quit_folder		; no more files in dir. 'cd ..'

	call	openDecompFile	; reads and opens the output file or makes a folder and goes in it
	
	jc		.ret
	
	cmp		DWORD [hossz], 1
	je		.lol
	
	call	decompTree 		; decompress. Do tree, while reading the file in
	
	call	actDecomp		; actual decode
	
	jmp		.lol
	
.ret:
	mov		eax, [fileHandle_in]
	call	[fio_close]
	ret
.quit_folder:
	cmp		DWORD [folderszam], 0
	je		.ret			; if we are not in a folder, the file's over
	
	; decrement the folderCounter
	mov		eax, DWORD [folderszam]
	dec		eax
	mov		DWORD [folderszam], eax
	
	; get back the fileCounter
	; pop		DWORD [szam]
	push	edx
	mov		ebx, 4
	mul		ebx
	add		eax, 1024
	mov		ebx, DWORD [char + eax]
	mov		DWORD [char + eax], 0		; nulling
	mov		DWORD [szam], ebx
	pop		edx
	
	push	directory
	push	256
	call	[GetCurrentDirectoryA]
	
	; change path
	; pop		eax		; length of prevDir
	push	edx
	mov		ebx, 4
	mov		eax, DWORD [folderszam]
	mul		ebx
	mov		edx, DWORD [char + eax]
	mov		DWORD [char + eax], 0		; nulling
	mov		eax, edx
	pop		edx
	
	pusha
	mov		ecx, eax
	xor		ebx, ebx
.masol:
	mov		edi, DWORD [edx + ebx]
	mov		DWORD [sequences + ebx], edi
	add		ebx, 4
	loop	.masol
	popa
	mov		DWORD [sequences + eax], 0
	
	push	sequences
	call	[SetCurrentDirectoryA]
	
	jmp		.lol
	
main:
	; READ
	call	[getargs]
	add		eax, 22
	movzx	ecx, BYTE [eax]
	sub		ecx, '0'
	test	ecx, ecx
	jz		.string
	cmp		ecx, 1
	je		.string
	; hiba bemenet
	; mov		eax, errormsg_read 		; doesnt work with alink
	; call	io_writestr
	
	jmp		.end

.string:
	add		eax, 2 	; to jump over 0/1 and space
	mov		[filename_in], eax
	
	; mov		eax, filename;_out ; to decompress

	; mov		ecx, 0
	
	test	ecx, ecx
	jz		.compress
	
	call	decompress
	jmp		.end
.compress:
	call	compress
.end:
	ret

.error:
	mov		eax, errormsg
	; call	io_writestr
	; mov		eax, 10
	; call	mio_writechar
	jmp		.end

	

section .data
Yofilename		db 'encodeDDDDD', 0
fileDecode		db '.decoded', 0
filename_out	db '.encoded', 0
file_search 	db '*', 0
errormsg		db 'Error opening file', 0
errormsg_read	db 'Error. Expected input: "huffman_compress.exe 0 <filename to be encoded>. 0, as in Compress, or 1 to Decompress', 0

section .bss
char			resd 65536 	; 256*256
file_attributes resd 512
sequences		resd 512
tree0			resd 512
tree1			resd 512
temp			resd 256
directory 		resd 256
filename		resd 16
filename_in		resd 16
string			resd 16
szam			resd 4
fileHandle_in  	resd 1
fileHandle_out 	resd 1
dirhandle		resd 1
min1 			resd 1
min2			resd 1
betu 			resd 1
hossz			resd 1
folderszam		resd 1
dunno			resd 1