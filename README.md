# Huffman Compressor

**Description**

The application compresses and decompresses files and directories using the Huffman algorithm.\
It was written in Assembly programming language.

The compressed file is created with a _.encoded_ ending,\
and the decompressed file is created with the same name as the original file, but the first letter of the filename is incremented by one.\
That is: _apple_ ---encode---> _apple.encoded_ ---decode---> _bpple_

---

**Usage**

`compile.bat` -- compiles the .asm file\
`huffman_copress.exe 0 <file>` -- to compress <file>\
`huffman_copress.exe 1 <file>.encoded` -- to decompress <file>.encoded

There is a test directory on which you can test the application: _apple_.

---

The project was done as an assignment for my _Computer architecture_ class, in my first semester of my Bachelor's degree of Computer Science, in Cluj-Napoca, Romania.

Student:
- Jánosi József-Hunor

:8ball:
